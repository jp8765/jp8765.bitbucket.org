
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var Itm = []; 
var cas = [];
var tmpitm =-1; 


function generirajPodatke(stPacienta) {
	$('#generirano').html("");
	
	var ehrId1 = "53026949-bc80-4f65-abbc-831fcf20ce15";

	var ehrId2 = "5a4c6e56-dd39-4002-85c2-d6153507e85a";

	var ehrId3 = "07e890e7-a8af-4957-b9e8-c492da7659b1";
	setTimeout(function(){$('#generirano').append("<p> Kreiran je nov EHR: " + ehrId2 + "</p>" )}, 500)
	setTimeout(function(){$('#generirano').append("<p> Kreiran je nov EHR: " + ehrId3 + "</p>" )}, 600)
	setTimeout(function(){$('#generirano').append("<p> Kreiran je nov EHR: " + ehrId1 + "</p>" )}, 700)
}
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();
	var spol = $("#kreirajSpol").val();
    //console.log("ime: " + ime + " priimek: " + priimek + " DR: "+ datumRojstva+ " spol: "+spol);
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0 || !spol) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            
		            partyAdditionalInfo: [
		                {key: "ehrId", value: ehrId} ,
		            {key: "sex",value: spol},
		            ]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();


	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function preberiMeritveVitalnihZnakov() {

	sessionId = getSessionId();
	var tip = $("#preberiTipZaVitalneZnake").val();
	var cilj = "weight";
	if (tip == "telesna višina") cilj = "height";
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var st = 0; 
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			var party = data.party;
			$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
      "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
      " " + party.lastNames + "'</b>.</span><br/><br/>");
			
				$.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + cilj,
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
				    	
					    	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Datum in ura</th>" + 
                "<th class='text-right'>Telesna "+tip.split(" ")[1]+"</th></tr>";
					        for (var i in res) {
					        cas[i] = res[i].time.split("T")[0];
		
					        if(i<10){
					            if (cilj=="weight")results += "<tr><td>" + res[i].time +
			                      "</td><td class='text-right'>" + res[i].weight + " " 	+
			                      res[i].unit + "</td>";
			                    if (cilj=="height")results += "<tr><td>" + res[i].time +
			                      "</td><td class='text-right'>" + res[i].height + " " 	+
			                      res[i].unit + "</td>";
					        }
					        }
					        results += "</table>";
					        $("#rezultatMeritveVitalnihZnakov").append(results);
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-warning fade-in'>" +
                "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
		
    	},
    	error: function(err) {
    		
    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
        JSON.parse(err.responseText).userMessage + "'!!");
    	}
	});
	setTimeout(function(){
    	analizapodatkov();
}, 500);

}
function analizapodatkov(){
		var Visine = [];
		var Teze = []; 
		var Cas = [];
		sessionId = getSessionId();
		var ehrId = $("#meritveVitalnihZnakovEHRid").val();
		$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			var party = data.party;
				$.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + "weight",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
					        for (var i in res) {
					        	if(i<10){
					        	Teze[i]=res[i].weight;
					        }
					      
				    	}}
				    },
				});
    	}
	});
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			var party = data.party;
				$.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + "height",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
					        for (var i in res) {
					        	if(i<10){
					        	Visine[i]=res[i].height;
					        }
					        
				    	}}
				    },
				});
    	}
	});
	
	setTimeout(function(){
		for(i in Visine){
    			Itm[i] = Teze[i]/Math.pow(Visine[i]/100,2);
    		}
    		
			tmpitm = (Teze[0]/Math.pow(Visine[0]/100, 2)).toFixed(2);
			if(tmpitm > 0){
			$("#Itm").html("<br/><span id='opozorilo'></b>.</span><br/><br/>"+	" <sscript src='https://code.highcharts.com/highcharts.js'></script>"+
			"<script src='https://code.highcharts.com/modules/exporting.js'></script>"+
			" <div id='container1' style='width:100%; height:400px;'></div>");
		
			if (tmpitm < 19,5){
				$('#opozorilo').html('<div class="alert alert-danger"> <strong>POZOR!</strong> Pacient je podhranjen!! </div>'+'<div class="alert alert-info"> <strong>PRIPOROČENO: </strong> Obisk najbljižje restavracije.</div>')
			}
				if (tmpitm > 19.5 && tmpitm<=25){
				$('#opozorilo').html('<div class="alert alert-success"> <strong>Super! </strong> Pacient ima odličen indeks telesne teže. </div>')
			}
			if (tmpitm > 25 && tmpitm<=30){
				$('#opozorilo').html('<div class="alert alert-warning"><strong>Pozor</strong> Pacient spada v 1. mejo debelosti. </div>'+'<div class="alert alert-info"> <strong>PRIPOROČENO: </strong> Obisk najbljižjega parka.</div>')
			}
			if (tmpitm > 30 && tmpitm<=30){
				$('#opozorilo').html('<div class="alert alert-warning"><strong>Pozor</strong> Pacient spada v 2. mejo debelosti! </div>'+'<div class="alert alert-info"> <strong>PRIPOROČENO: </strong> Obisk najbljižjega parka ali telovadnice.</div>')
			}
			if (tmpitm > 40){
				$('#opozorilo').html('<div class="alert alert-danger"> <strong>POZOR!</strong> Pacient spada v 3. mejo debelosti!! </div>'+'<div class="alert alert-info"> <strong>PRIPOROČENO: </strong> Obisk najbljižjega parka ali telovadnice.</div>')
			}
			           
    		 fillGraph();
    		 if(tmpitm<20 || tmpitm>25){
    		 
    		 	$("#googleHelp").html(
			'<div class="panel panel-primary text-left" style="float: left; align= center; width: 44% ; margin: 3% ">'+
        	'<div class="panel-collapse collapse in" id="UstvariZapis" aria-expanded="true" style="">'+
        	'<div class="panel-body" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)">'+
        	'<div id="map" style =" width: 100%; height: 400px; background-color: grey;"></div>'+
        	'</div></div></div></div>'
        	
			) ; 	fillMap();
    		 	
    		 }
			}
			
	}, 1000);
}
function fillMap(){
	 initMap();

}
	
var map;
      var infowindow;
 
      function initMap() {
        var ljubljana = {lat:46.0496869, lng: 14.5066457};
       

        map = new google.maps.Map(document.getElementById('map'), {
          center: ljubljana,
          zoom: 15
        });

        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        if(tmpitm > 40){
        	service.nearbySearch({
          location: ljubljana,
          radius: 2000,
          type: ['gym']
        }, callback);
        service.nearbySearch({
          location: ljubljana,
          radius: 2000,
          type: ['park']
        }, callback);
        } 
         if(tmpitm <20)
        service.nearbySearch({
          location: ljubljana,
          radius: 2000,
          type: ['restaurant']
        }, callback);
      }

      function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
      }

      function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(place.name);
          infowindow.open(map, this);
        });
      }
function fillGraph(){
	var podhran = []; 
	var debelost1 = []; 
	var debelost2 = []; 
	var debelost3 = []; 
	
	
	for(i in cas) {
		podhran[i] = 19,5; 
		debelost1[i]= 25;
		debelost2[i]= 30;
		debelost3[i]= 40; 
	}
	cas = cas.reverse();
	
	Itm = Itm.reverse();

$(function () { Highcharts.chart('container1', {
	
    title: {
        text:  'Indeks telesne mase pacienta je: '+ tmpitm + "."
    },

    
	xAxis: {
    categories: cas
},
    yAxis: {
        title: {
            text: 'Indeks Telesne Mase'
        }
    },
    series: [
    {    name: 'ITM',
		lineWidth: 5,
		color: '#1E90FF',
    	data: Itm
    
    	
    },
   {	name: 'Meja podhranjenosti',
	    color: '#DC143C',
		data: podhran
		
   	
   },
   {	name: 'Meja prve debelosti',
		lineWidth: 1,
		data: debelost1,
   		color: '#7CFC00'
   },
   {	name: 'Meja druge debelosti',
		lineWidth: 1,
		color: '#FF8C00',
		data: debelost2
   	
   },
   {	name: 'Meja tretje debelosti',
		color: '#8B0000',
		data: debelost3
   	 	
   }

   ]

});
});
}